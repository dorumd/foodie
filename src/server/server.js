import Express from 'express';
import React from 'react';
import Router from 'react-router';
import Routes from '../shared/routes';
const app = Express();

// set up Jade
app.set('views', './views');
app.set('view engine', 'jade');

app.get('/*', function (req, res) {
    Router.run(Routes, req.url, Handler => {
        let content = React.renderToString(<Handler />);
        res.render('index', {content: content});
    });
});

var server = app.listen(3000, function () {
    var host = server.address().address,
        port = server.address().port;

    console.log('App listening at http://%s:%s', host, port);
});
