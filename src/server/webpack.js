import WebpackDevServer from 'webpack-dev-server';
import Webpack from 'webpack';
import Config from '../../webpack.config.js';

var server = new WebpackDevServer(webpack(config), {
    // webpack-dev-server options
    publicPath: config.output.publicPath,
    hot: true,
    stats: {colors: true},
});

server.listen(8080, "localhost", function() {});
