import { Route, DefaultRoute } from "react-router";
import React from "react";

import AppHandler from "./components/AppHandler";
import AboutPageHandler from './components/about';

export default (
    <Route path="/" handler={AppHandler}>
        <DefaultRoute name="home" handler={AboutPageHandler}/>
        <Route name="about" handler={AboutPageHandler} path="/about" />
    </Route>
);
