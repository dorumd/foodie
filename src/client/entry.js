import React from 'react';
import Router from 'react-router';
import Routes from '../shared/routes';

Router.run(Routes, Router.HistoryLocation, (Handler, state) => {
    React.render(<Handler />, document.getElementById('app'));
});
